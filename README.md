# Go Applications

## Notes

- `go install <app name>` installs the binary.
- `go clean -i` removes the binary corresponding to the current dir.
- The `Go workspace` dir is the `/go` directory. It contains the `src` and `bin` directories.
- The `Go workspace` contains `repositories`
- A `repository` contains one or more `packages`
- A `package` consists of one or more `Go` source files in a single directory.
- The path to a packages directory determines it's import path.

`Workspace` (`/go`) -> `repos` -> `packages` -> `subdir in package`

For example:
```
bin/
    hello                          # command executable
    outyet                         # command executable
src/
    github.com/golang/example/
        .git/                      # Git repository metadata
    hello/
        hello.go               # command source
    outyet/
        main.go                # command source
        main_test.go           # test source
    stringutil/
        reverse.go             # package source
        reverse_test.go        # test source
    golang.org/x/image/
        .git/                      # Git repository metadata
    bmp/
        reader.go              # package source
        writer.go              # package source
```

---

## Links
- [How to Write Go Code](https://golang.org/doc/code.html)
-