FROM golang:1.11.0-stretch

WORKDIR /go/src
RUN apt-get update && apt-get -y install vim && \
        mkdir -p ~/.vim/autoload ~/.vim/bundle && \
        curl -LSso ~/.vim/autoload/pathogen.vim https://tpo.pe/pathogen.vim && \
        git clone https://github.com/fatih/vim-go.git ~/.vim/bundle/vim-go && \
        git clone https://github.com/flazz/vim-colorschemes ~/.vim/bundle/vim-colorschemes.vim && \
        git clone https://github.com/itchyny/lightline.vim ~/.vim/bundle/lightline.vim &&\
        git clone https://github.com/joshdick/onedark.vim ~/.vim/bindle/onedark.vim


COPY dotfiles/.vimrc /root/.vimrc
RUN echo "alias ll='ls -lahS | grep "^d" && ls -lahS | grep -v \"^d\"'" >> ~/.bashrc
RUN echo "alias la='ls -A'" >> ~/.bashrc
RUN echo "alias l='ls -CF'" >> ~/.bashrc

# RUN go get -d -v ./...
# RUN go install -v ./...

# CMD ["app"]