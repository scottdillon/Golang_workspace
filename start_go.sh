#! /bin/bash

docker run \
    -it \
    --rm \
    --name golang-container \
    --mount type=bind,source="$(pwd)"/src,target=/go/src \
    go-1.11-container:latest